// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "FishState.generated.h"


/////////////////////////////////////////////////////////////////////////////
//////////////                                        //////////////////////            
//////////////            Fish State		          /////////////////////                                 
//////////////                                        ////////////////////              
/////////////////////////////////////////////////////////////////////////
UCLASS()
class URBANWHALE_API UFishState : public UObject
{
	GENERATED_BODY()

protected:
	AFlockFish* Fish;

public:
	// Constructors
	FishState() {}
	FishState(AFlockFish* aFish)
	{
		Fish = aFish;
	};
	virtual void Update(float val) {};
    void HandleOverlap(AActor *otherActor, UPrimitiveComponent *otherComponent, FString aColliderString) {};
	
};


/////////////////////////////////////////////////////////////////////////////
//////////////                                        //////////////////////            
//////////////            Fish Seek State             /////////////////////                                 
//////////////                                        ////////////////////              
/////////////////////////////////////////////////////////////////////////

class SeekState : public FishState
{
public:

	SeekState(AFlockFish* aFish) : FishState(aFish) {};
	virtual void Update(float delta) override;

protected:

	virtual void SeekTarget(float delta);
	virtual void Flock(float delta);
	virtual void Shoal(float delta);
};


/////////////////////////////////////////////////////////////////////////////
//////////////                                        //////////////////////            
//////////////            Fish Flee State             /////////////////////                                 
//////////////                                        ////////////////////              
/////////////////////////////////////////////////////////////////////////

class OCEANDEMO_API FleeState : public FishState
{
protected:
	AActor* Enemy;

public:

	FleeState(AFlockFish* aFish, AActor* aEnemy) : FishState(aFish)
	{
		Enemy = aEnemy;
	};
	virtual void Update(float delta) override;

protected:

	virtual void FleeFromEnemy(float delta);

};


/////////////////////////////////////////////////////////////////////////////
//////////////                                        //////////////////////            
//////////////            Fish Chase State            /////////////////////                                 
//////////////                                        ////////////////////              
/////////////////////////////////////////////////////////////////////////

class OCEANDEMO_API ChaseState : public FishState
{
protected:
	AActor* Prey;

public:

	ChaseState(AFlockFish* aFish, AActor* aPrey) : FishState(aFish)
	{
		Prey = aPrey;
	};
	virtual void Update(float delta) override;

protected:

	virtual void ChasePrey(float delta);
	virtual void EatPrey();
};

